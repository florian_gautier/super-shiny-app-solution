package my.company.app.lib;

public class MyService {
    
    /**
     * What a method !
     */
    public int doAdd(int a, int b)
    {
        return a+b;
    }

    /**
     * What a method !
     */
    public int doMult(int a, int b)
    {
        return a*b;
    }

    /**
     * What a method !
     */
    public int doSub(int a, int b)
    {
        return a-b;
    }

        /**
     * What a method !
     */
    public int doDiv(int a, int b)
    {
        return a/b;
    }
}

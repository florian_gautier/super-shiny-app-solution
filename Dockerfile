FROM maven:3.6-adoptopenjdk-11

WORKDIR /root
COPY target/super-shiny-app-1.0-SNAPSHOT.jar .
ENTRYPOINT [ "java","-cp", "super-shiny-app-1.0-SNAPSHOT.jar", "my.company.app.App" ]